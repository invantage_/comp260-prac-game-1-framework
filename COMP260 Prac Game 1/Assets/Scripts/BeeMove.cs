﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	public ParticleSystem explosionPrefab;

	// public parameters with default values
	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	// private state
	private float speed;
	private float turnSpeed;
	private Transform target1;
	private Transform target2;
	private Vector2 heading = Vector3.right;

	// Use this for initialization
	void Start () {
		// find a player object to be the target by type
		PlayerMove player = (PlayerMove) FindObjectOfType(typeof(PlayerMove));
		target1 = player.transform;
		target2 = player.transform;

		// bee initially moves in random direction
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate(angle);

		// set speed and turnSpeed randomly 
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, Random.value);
	}
	
	// Update is called once per frame
	void Update () {
		// get the vector from the bee to both targets 
		Vector2 direction1 = target1.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;
		Vector2 directionMain;

		//determine which target is closest and move pick a direction
		if (direction1.magnitude < direction2.magnitude) {
			directionMain = direction1;
		} else {
			directionMain = direction2;
		}

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (directionMain.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 directionMain = target1.position - transform.position;
		Gizmos.DrawRay(transform.position, directionMain);
	}

	void OnDestroy() {
		// create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		// destroy the particle system when it is over
		Destroy(explosion.gameObject, explosion.duration);
	}
}
