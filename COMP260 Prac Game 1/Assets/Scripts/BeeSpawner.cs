﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

	public int nBees = 5;
	public BeeMove beePrefab;
	public float xMin, yMin;
	public float width, height;

	private float beePeriod = 5.0f;
	public float minBeePeriod = 1.0f;
	public float maxBeePeriod = 10.0f;

	private int beeCounter = 0;

	void Start () {
		// create bees
		for (beeCounter = 0; beeCounter < nBees; beeCounter++) {
			// instantiate a bee
			BeeMove bee = Instantiate (beePrefab);
			// attach to this object in the hierarchy
			bee.transform.parent = transform;            
			// give the bee a name and number
			bee.gameObject.name = "Bee " + beeCounter;

			// move the bee to a random position within 
			// the bounding rectangle
			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2(x,y);
		}
	}

	public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within ‘radius’ of ‘centre’
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);

			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy(child.gameObject);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		beePeriod -= Time.deltaTime;

		if (beePeriod <= 0.0f) {
			// instantiate a bee
			BeeMove bee = Instantiate (beePrefab);
			// attach to this object in the hierarchy
			bee.transform.parent = transform;            
			// give the bee a name and number
			bee.gameObject.name = "Bee " + beeCounter;
			beeCounter++;

			// move the bee to a random position within 
			// the bounding rectangle
			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2(x,y);

			beePeriod = Random.Range(minBeePeriod, maxBeePeriod);
		}
	}
}
